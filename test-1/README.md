## TODO

- [x] Type checking (`let` expressions).
- [ ] Type inference (`let` expressions).
- [ ] String interpolation.
- [ ] List literals.
- [ ] Tuple literals.
- [ ] Map literals.
- [ ] Functions.
