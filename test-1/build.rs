use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {

    // Process LALRPOP files.
    lalrpop::Configuration::new()
        .generate_in_source_tree()
        .process()?;

    Ok(())
}