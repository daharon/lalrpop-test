use std::collections::HashMap;
use std::error::Error;
use std::fmt;

use crate::ast::{BinOp, Expression, Literal, TopLevel, UnaryOp};
use crate::types::{PrimitiveType, Type};

#[derive(Debug)]
pub struct TypeCheckError(String);

impl Error for TypeCheckError {}

impl fmt::Display for TypeCheckError {
    fn fmt(&self, f: &mut fmt::Formatter) -> std::fmt::Result {
        write!(f, "Error:  {}", self.0)
    }
}

/// The `_` identifier, which we should not be added to the environment.
const UNDERSCORE_IDENTIFIER: &str = "_";

struct Env {
    pub identifiers: HashMap<String, Type>,
}

/// Type checking and inference for the input AST.
pub fn unify(ast: &Vec<TopLevel>) -> Result<Vec<TopLevel>, TypeCheckError> {
    let mut typed_ast: Vec<TopLevel> = Vec::new();
    for top_level in ast {
        let typed = match top_level {
            TopLevel::Comment(c) => TopLevel::Comment(c.clone()),
            TopLevel::LetBinding { identifier, t: explicit_t, expression } => {
                let mut env = Env { identifiers: HashMap::new() };
                let expr_t = type_expression(expression, &mut env)?;
                if let Some(typ) = explicit_t {
                    if *typ != expr_t {
                        return Err(TypeCheckError(format!("Mismatched types; Expected `{}`, found `{}`", typ, expr_t)));
                    }
                }
                if identifier != UNDERSCORE_IDENTIFIER {
                    env.identifiers.insert(identifier.into(), expr_t.clone());
                }
                TopLevel::LetBinding {
                    identifier: identifier.clone(),
                    t: Some(expr_t),
                    expression: expression.clone(),
                }
            }
        };
        typed_ast.push(typed);
    }
    Ok(typed_ast)
}

/// Recursively deduce the type of the expression.
fn type_expression(expr: &Expression, env: &mut Env) -> Result<Type, TypeCheckError> {
    match expr {
        Expression::Literal(lit) => {
            match lit {
                Literal::Unit => Ok(Type::Primitive(PrimitiveType::Unit)),
                Literal::String(_) => Ok(Type::Primitive(PrimitiveType::String)),
                Literal::Integer(_) => Ok(Type::Primitive(PrimitiveType::Integer)),
                Literal::Float(_) => Ok(Type::Primitive(PrimitiveType::Float)),
                Literal::Boolean(_) => Ok(Type::Primitive(PrimitiveType::Boolean)),
            }
        }
        Expression::Identifier(ident) => {
            match env.identifiers.get(ident) {
                None => Err(TypeCheckError(format!("Cannot find symbol `{}`", ident))),
                Some(t) => Ok(t.clone()),
            }
        }
        Expression::UnaryOp { op, operand } => {
            let t = type_expression(operand, env)?;
            match op {
                UnaryOp::Not => {
                    match &t {
                        Type::Primitive(pt) => {
                            match pt {
                                PrimitiveType::Boolean => Ok(t),
                                _ => Err(TypeCheckError(format!("Cannot use `!` unary operator on type `{:?}`", t))),
                            }
                        }
                        _ => Err(TypeCheckError(format!("Cannot use `!` unary operator on type `{:?}`", t))),
                    }
                }
                UnaryOp::Negative => {
                    match &t {
                        Type::Primitive(pt) => {
                            match pt {
                                PrimitiveType::Integer
                                | PrimitiveType::Float => Ok(t),
                                _ => Err(TypeCheckError(format!("Cannot use `-` unary operator on type `{:?}`", t))),
                            }
                        }
                        _ => Err(TypeCheckError(format!("Cannot use `-` unary operator on type `{:?}`", t))),
                    }
                }
            }
        }
        Expression::BinOp { op, left, right } => {
            let lht = type_expression(left, env)?;
            let rht = type_expression(right, env)?;
            use Type::Primitive;
            use PrimitiveType::{Boolean, Float, Integer};
            match op {
                BinOp::Addition
                | BinOp::Subtraction
                | BinOp::Division
                | BinOp::Multiplication
                | BinOp::Modulus
                | BinOp::Exponentiation =>
                    match (&lht, &rht) {
                        (Primitive(Integer), Primitive(Integer)) =>
                            Ok(Primitive(Integer)),
                        (Primitive(Float), Primitive(Float)) =>
                            Ok(Primitive(Float)),
                        (_, _) =>
                            Err(TypeCheckError(format!("Cannot use `{:?}` binary operator on types `{:?}` and `{:?}`", op, lht, rht))),
                    },
                BinOp::Greater
                | BinOp::Lesser
                | BinOp::GreaterEqual
                | BinOp::LesserEqual =>
                    match (&lht, &rht) {
                        (Primitive(Integer), Primitive(Integer))
                        | (Primitive(Float), Primitive(Float)) =>
                            Ok(Primitive(Boolean)),
                        (_, _) =>
                            Err(TypeCheckError(format!("Cannot use `{:?}` binary operator on types `{:?}` and `{:?}`", op, lht, rht))),
                    },
                BinOp::Equal
                | BinOp::NotEqual =>
                    if lht == rht {
                        Ok(Primitive(Boolean))
                    } else {
                        Err(TypeCheckError(format!("Cannot use `{:?}` binary operator on dissimilar types `{:?}` and `{:?}`", op, lht, rht)))
                    },
                BinOp::And
                | BinOp::Or =>
                    match (&lht, &rht) {
                        (Primitive(Boolean), Primitive(Boolean)) =>
                            Ok(Primitive(Boolean)),
                        (_, _) =>
                            Err(TypeCheckError(format!("Cannot use `{:?}` binary operator on types `{:?}` and `{:?}`", op, lht, rht))),
                    },
            }
        }
        Expression::LetBinding { identifier, t, expression, sub_expression } => {
            let expr_t = type_expression(expression, env)?;
            if let Some(typ) = t {
                if *typ != expr_t {
                    return Err(TypeCheckError(format!("Mismatched types; Expected `{}`, found `{}`", typ, expr_t)));
                }
            }
            if identifier != UNDERSCORE_IDENTIFIER {
                env.identifiers.insert(identifier.into(), expr_t.clone());
            }
            let sub_expr_t = type_expression(sub_expression, env)?;
            Ok(sub_expr_t)
        }
    }
}
