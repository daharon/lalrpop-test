//! Abstract syntax tree structs.

use crate::types::Type;

#[derive(Clone, Debug, PartialEq)]
pub enum Comment {
    LineComment(String),
    BlockComment(String),
    DocComment(String),
}

pub type Identifier = String;

#[derive(Clone, Debug, PartialEq)]
pub enum Literal {
    Unit,
    String(String),
    Integer(i64),
    Float(f64),
    Boolean(bool),
}

#[derive(Clone, Debug, PartialEq)]
pub enum TopLevel {
    Comment(Comment),
    LetBinding {
        identifier: String,
        /// Explicit type of expression.
        t: Option<Type>,
        expression: Expression,
    },
}

#[derive(Clone, Debug, PartialEq)]
pub enum BinOp {
    // Numbers
    Addition,
    Subtraction,
    Division,
    Multiplication,
    Modulus,
    Exponentiation,

    // Booleans
    Equal,
    NotEqual,
    Greater,
    Lesser,
    GreaterEqual,
    LesserEqual,
    And,
    Or,
}

#[derive(Clone, Debug, PartialEq)]
pub enum UnaryOp {
    Not,
    Negative,
}

#[derive(Clone, Debug, PartialEq)]
pub enum Expression {
    Literal(Literal),
    Identifier(Identifier),
    UnaryOp {
        op: UnaryOp,
        operand: Box<Expression>,
    },
    BinOp {
        op: BinOp,
        left: Box<Expression>,
        right: Box<Expression>,
    },
    LetBinding {
        identifier: String,
        /// Explicit type of expression.
        t: Option<Type>,
        expression: Box<Expression>,
        sub_expression: Box<Expression>,
    },
}
