use crate::ast::{BinOp, Comment, Expression, Literal, TopLevel, UnaryOp};
use crate::types::Type;

pub fn generate_rust_source(top_levels: &Vec<TopLevel>) -> String {
    top_levels.iter()
        .flat_map(|top_level| {
            match top_level {
                TopLevel::Comment(comment) => {
                    match comment {
                        Comment::LineComment(_)
                        | Comment::BlockComment(_) => None,
                        Comment::DocComment(comment) => Some(comment.into()),
                    }
                }
                TopLevel::LetBinding { identifier, t, expression } =>
                    Some(let_binding_to_rust(&identifier, &expression, t)),
            }
        })
        .collect::<Vec<String>>()
        .join("\n")
}

fn let_binding_to_rust(identifier: &str, expression: &Expression, t: &Option<Type>) -> String {
    let left = format!("let {}", identifier);
    let typ = match t {
        None => "".to_string(),
        Some(t) => format!(": {}", t.to_rust_type()),
    };
    let right = expression_to_rust(expression);
    format!("{}{} = {{ {} }};", left, typ, right)
}

fn expression_to_rust(expr: &Expression) -> String {
    fn rust_binop(op: &str, left: &Expression, right: &Expression) -> String {
        format!("({}) {} ({})", expression_to_rust(left), op, expression_to_rust(right))
    }
    match expr {
        Expression::Identifier(val) => val.to_string(),
        Expression::Literal(val) => {
            match val {
                Literal::Unit => String::from("()"),
                Literal::String(val) => format!("\"{}\".to_string()", val),
                Literal::Integer(val) => val.to_string(),
                Literal::Float(val) => format!("{}f64", val),
                Literal::Boolean(val) => match val {
                    true => String::from("true"),
                    false => String::from("false"),
                },
            }
        }
        Expression::BinOp { op, left, right } => {
            match op {
                BinOp::Addition => rust_binop("+", left, right),
                BinOp::Subtraction => rust_binop("-", left, right),
                BinOp::Division => rust_binop("/", left, right),
                BinOp::Multiplication => rust_binop("*", left, right),
                BinOp::Modulus => rust_binop("%", left, right),
                BinOp::Exponentiation => {
                    format!("({}).pow({})",
                            expression_to_rust(left),
                            expression_to_rust(right))
                }
                BinOp::Equal => rust_binop("==", left, right),
                BinOp::NotEqual => rust_binop("!=", left, right),
                BinOp::Greater => rust_binop(">", left, right),
                BinOp::Lesser => rust_binop("<", left, right),
                BinOp::GreaterEqual => rust_binop(">=", left, right),
                BinOp::LesserEqual => rust_binop("<=", left, right),
                BinOp::And => rust_binop("&&", left, right),
                BinOp::Or => rust_binop("||", left, right),
            }
        }
        Expression::UnaryOp { op, operand } => {
            match op {
                UnaryOp::Not => format!("!({})", expression_to_rust(operand)),
                UnaryOp::Negative => format!("-({})", expression_to_rust(operand)),
            }
        }
        Expression::LetBinding { identifier, t, expression, sub_expression } => {
            let binding = let_binding_to_rust(identifier, expression, t);
            let sub_expr = expression_to_rust(sub_expression);
            format!("{} {{ {} }}", binding, sub_expr)
        }
    }
}