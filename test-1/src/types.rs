use std::fmt;

#[derive(Clone, Debug, PartialEq)]
pub enum PrimitiveType {
    Unit,
    Integer,
    Float,
    String,
    Boolean,
}

impl fmt::Display for PrimitiveType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let t = match self {
            PrimitiveType::Unit => "Unit",
            PrimitiveType::Integer => "Int.t",
            PrimitiveType::Float => "Float.t",
            PrimitiveType::String => "String.t",
            PrimitiveType::Boolean => "Bool.t",
        };
        write!(f, "{}", t)
    }
}

impl PrimitiveType {
    pub fn to_rust_type(&self) -> String {
        let s = match self {
            PrimitiveType::Unit => "()",
            PrimitiveType::Integer => "i64",
            PrimitiveType::Float => "f64",
            PrimitiveType::String => "String",
            PrimitiveType::Boolean => "bool",
        };
        String::from(s)
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum Type {
    Primitive(PrimitiveType),
    Identifier(String),
}

impl fmt::Display for Type {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let ts = match self {
            Type::Primitive(pt) => format!("{}", pt),
            Type::Identifier(ident) => ident.into(),
        };
        write!(f, "{}", ts)
    }
}

impl<T: AsRef<str>> From<T> for Type {
    fn from(t: T) -> Self {
        match t.as_ref() {
            "()" => Type::Primitive(PrimitiveType::Unit),
            "Bool.t" => Type::Primitive(PrimitiveType::Boolean),
            "Int.t" => Type::Primitive(PrimitiveType::Integer),
            "Float.t" => Type::Primitive(PrimitiveType::Float),
            "String.t" => Type::Primitive(PrimitiveType::String),
            non_primitive => Type::Identifier(non_primitive.to_string()),
        }
    }
}

impl Type {
    pub fn to_rust_type(&self) -> String {
        match self {
            Type::Primitive(pt) => pt.to_rust_type(),
            Type::Identifier(ident) => ident.into(),
        }
    }
}