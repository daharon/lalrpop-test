mod types;
mod type_env;
mod unifier;
mod grammar;
mod generate;
mod ast;

use crate::unifier::unify;
use crate::ast::TopLevel;
use crate::generate::generate_rust_source;

fn main() {
    vec![
        "let x = 2 + 2",
        "let x = 2.0 + 2",
        "let x = 2.0 + 2 * 3",
        "let x = (2 + -2) - 5",
        "let x = (2 + 2) - -5 * 10.0",
        "let x = 9.0 + 2 * 3 - 2 ** 2",
        "let x = \"Hello, World!\"",
        "let x = true",
        "let x = 0x123",
        "let x = -0x123",
        "let x = 0o17",
        "let x = -0o17",
        "let x = 0b1010",
        "let x = -0b1010",
        "
            let x = true
            let y = false
            let z =
                (9 + 7 * 3) ** 3
        ",
        "let x = !(5 > 3)",
        "let x = 5 > 3 && 7 <= 10",
        "let x = 5 > 3 && 7 <= 10 || -45 >= -101",
        "
            let some_bool = true
            let some_int = 45
            let some_other_int = 187
            let x = !some_bool && some_int > some_other_int
        ",
        "let x = 9 % 3 + 144 % 45",
        "
            // A line comment
            let z = 4
            /// A documentation comment
            let y = 3
            /* A block comment */
            let x = 2
            let a = 5 // Line comment at end
            let b = 6
        ",
        "
            let w: Float.t = 5.0
            let x
                : Int.t = 5
            let y: Bool.t = false
            let z: String.t = \"Hello, World!\"
            //let a: Bool.t = 5
        ",
        "
            let _ = 5
            let _: Bool.t = true
        ",
        "let x: Custom.t = 5",
        "
            let x: Int.t =
                let y = 5 in
                let z = y + 2 in
                z ** 2
        ",
        "let y: () = something_that_returns_unit",
        "let y = ()",
    ].into_iter()
        .for_each(test_source)
}

fn test_source(source: &str) {
    println!("---");
    println!("Source:");
    println!("{}", source);
    // Parse and build abstract syntax tree.
    let top_levels: Vec<TopLevel> = grammar::TopLevelsParser::new()
        .parse(source).unwrap();
    println!("AST:  {:?}", top_levels);
    // Perform type checking.
    let typed_top_levels = match unify(&top_levels) {
        Ok(result) => {
            println!("Typed AST:  {:?}", result);
            result
        }
        Err(e) => {
            println!("Error typing the AST:  {}", e);
            return;
        }
    };
    // Generate Rust source.
    println!("Generated Rust source:");
    println!("{}", generate_rust_source(&typed_top_levels));
}

#[cfg(test)]
mod parser {
    use crate::ast::TopLevel;
    use crate::grammar;

    fn parse_source(source: &str) -> Vec<TopLevel> {
        grammar::TopLevelsParser::new()
            .parse(source)
            .unwrap()
    }

    mod top_levels {
        use super::parse_source;

        /// Should fail with invalid identifier that starts with an uppercase letter.
        #[test]
        #[ignore]
        fn parse_failure() {
            let test_source = "let A_bc = 123";
            let _error = parse_source(test_source);
        }

        #[test]
        fn empty() {
            let test_source = "";
            let expected = 0;
            let top_levels = parse_source(test_source);
            assert_eq!(expected, top_levels.len())
        }

        #[test]
        fn single() {
            let test_source = "let x = 2";
            let expected = 1;
            let top_levels = parse_source(test_source);
            assert_eq!(expected, top_levels.len())
        }

        #[test]
        fn multiple() {
            let test_source = "\
                let x = 2\
                let y = 10.3\
                let z = false\
            ";
            let expected = 3;
            let top_levels = parse_source(test_source);
            assert_eq!(expected, top_levels.len())
        }
    }

    mod literal {
        use crate::ast::{Expression, Literal, TopLevel, UnaryOp};
        use super::parse_source;
        use crate::types::{Type, PrimitiveType};

        #[test]
        fn literal_integer() {
            let test_source = "let x = 2";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::Literal(Literal::Integer(2)),
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn literal_negative_integer() {
            let test_source = "let x = -2";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::UnaryOp {
                        op: UnaryOp::Negative,
                        operand: Expression::Literal(Literal::Integer(2)).into(),
                    },
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn literal_integer_hex() {
            let test_source = "let x = 0xA";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::Literal(Literal::Integer(10)),
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn literal_negative_integer_hex() {
            let test_source = "let x = -0xA";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::UnaryOp {
                        op: UnaryOp::Negative,
                        operand: Expression::Literal(Literal::Integer(10)).into(),
                    },
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn literal_integer_octal() {
            let test_source = "let x = 0o17";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::Literal(Literal::Integer(15)),
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn literal_negative_integer_octal() {
            let test_source = "let x = -0o17";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::UnaryOp {
                        op: UnaryOp::Negative,
                        operand: Expression::Literal(Literal::Integer(15)).into(),
                    },
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn literal_integer_binary() {
            let test_source = "let x = 0b1010";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::Literal(Literal::Integer(10)),
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn literal_negative_integer_binary() {
            let test_source = "let x = -0b1010";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::UnaryOp {
                        op: UnaryOp::Negative,
                        operand: Expression::Literal(Literal::Integer(10)).into(),
                    },
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn literal_float() {
            let test_source = "let x = 123.0";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::Literal(Literal::Float(123.0)),
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn literal_negative_float() {
            let test_source = "let x = -123.0";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::UnaryOp {
                        op: UnaryOp::Negative,
                        operand: Expression::Literal(Literal::Float(123.0)).into(),
                    },
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn literal_string() {
            let test_source = "let x = \"test\"";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::Literal(Literal::String("test".into())),
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn literal_utf8_string() {
            let test_source = "let x = \"אבג\"";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::Literal(Literal::String("אבג".into())),
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn literal_empty_string() {
            let test_source = "let x = \"\"";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::Literal(Literal::String("".into())),
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn literal_boolean_true() {
            let test_source = "let x = true";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::Literal(Literal::Boolean(true)),
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn literal_boolean_false() {
            let test_source = "let x = false";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::Literal(Literal::Boolean(false)),
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn literal_unit_primitive() {
            let test_source = "
                let x = ()
                let y: () = ()
            ";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::Literal(Literal::Unit),
                },
                TopLevel::LetBinding {
                    identifier: "y".into(),
                    t: Some(Type::Primitive(PrimitiveType::Unit)),
                    expression: Expression::Literal(Literal::Unit),
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }
    }

    mod unary_operation {
        use crate::ast::{Expression, Literal, TopLevel, UnaryOp};
        use super::parse_source;

        #[test]
        fn unary_operation_not() {
            let test_source = "let x = !true";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::UnaryOp {
                        op: UnaryOp::Not,
                        operand: Expression::Literal(Literal::Boolean(true)).into(),
                    },
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn unary_operation_negative() {
            let test_source = "let x = -123.456";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::UnaryOp {
                        op: UnaryOp::Negative,
                        operand: Expression::Literal(Literal::Float(123.456)).into(),
                    },
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }
    }

    mod binary_operation {
        use crate::ast::{BinOp, Expression, Literal, TopLevel, UnaryOp};
        use super::parse_source;

        #[test]
        fn binary_operation_simple_addition() {
            let test_source = "let x = 2 + 2";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::BinOp {
                        op: BinOp::Addition,
                        left: Expression::Literal(Literal::Integer(2)).into(),
                        right: Expression::Literal(Literal::Integer(2)).into(),
                    },
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn binary_operation_simple_multiplication() {
            let test_source = "let x = 2 * 2";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::BinOp {
                        op: BinOp::Multiplication,
                        left: Expression::Literal(Literal::Integer(2)).into(),
                        right: Expression::Literal(Literal::Integer(2)).into(),
                    },
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn binary_operation_simple_exponentiation() {
            let test_source = "let x = 2 ** 2";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::BinOp {
                        op: BinOp::Exponentiation,
                        left: Expression::Literal(Literal::Integer(2)).into(),
                        right: Expression::Literal(Literal::Integer(2)).into(),
                    },
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn binary_operation_precedence() {
            let test_source = "let x = 9.0 + 2 * 3 - 2 ** 2";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::BinOp {
                        op: BinOp::Subtraction,
                        left: Expression::BinOp {
                            op: BinOp::Addition,
                            left: Expression::Literal(Literal::Float(9.0)).into(),
                            right: Expression::BinOp {
                                op: BinOp::Multiplication,
                                left: Expression::Literal(Literal::Integer(2)).into(),
                                right: Expression::Literal(Literal::Integer(3)).into(),
                            }.into(),
                        }.into(),
                        right: Expression::BinOp {
                            op: BinOp::Exponentiation,
                            left: Expression::Literal(Literal::Integer(2)).into(),
                            right: Expression::Literal(Literal::Integer(2)).into(),
                        }.into(),
                    },
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn binary_operation_parenthesis() {
            let test_source = "let x = (2 + 2) - 5";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::BinOp {
                        op: BinOp::Subtraction,
                        left: Expression::BinOp {
                            op: BinOp::Addition,
                            left: Expression::Literal(Literal::Integer(2)).into(),
                            right: Expression::Literal(Literal::Integer(2)).into(),
                        }.into(),
                        right: Expression::Literal(Literal::Integer(5)).into(),
                    },
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn binary_operation_parenthesis_and_precedence() {
            let test_source = "let x = (2 + 2) - 5 / 10.0";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::BinOp {
                        op: BinOp::Subtraction,
                        left: Expression::BinOp {
                            op: BinOp::Addition,
                            left: Expression::Literal(Literal::Integer(2)).into(),
                            right: Expression::Literal(Literal::Integer(2)).into(),
                        }.into(),
                        right: Expression::BinOp {
                            op: BinOp::Division,
                            left: Expression::Literal(Literal::Integer(5)).into(),
                            right: Expression::Literal(Literal::Float(10.0)).into(),
                        }.into(),
                    },
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn binary_operation_modulus() {
            let test_source = "let x = 9 % 2 + 144 % 47";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::BinOp {
                        op: BinOp::Addition,
                        left: Expression::BinOp {
                            op: BinOp::Modulus,
                            left: Expression::Literal(Literal::Integer(9)).into(),
                            right: Expression::Literal(Literal::Integer(2)).into(),
                        }.into(),
                        right: Expression::BinOp {
                            op: BinOp::Modulus,
                            left: Expression::Literal(Literal::Integer(144)).into(),
                            right: Expression::Literal(Literal::Integer(47)).into(),
                        }.into(),
                    },
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn binary_operation_boolean_and() {
            let test_source = "let x = true && false";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::BinOp {
                        op: BinOp::And,
                        left: Expression::Literal(Literal::Boolean(true)).into(),
                        right: Expression::Literal(Literal::Boolean(false)).into(),
                    },
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn binary_operation_boolean_or() {
            let test_source = "let x = true || false";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::BinOp {
                        op: BinOp::Or,
                        left: Expression::Literal(Literal::Boolean(true)).into(),
                        right: Expression::Literal(Literal::Boolean(false)).into(),
                    },
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn binary_operation_boolean_and_or_precedence() {
            let test_source = "let x = true && !true || false";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::BinOp {
                        op: BinOp::Or,
                        left: Expression::BinOp {
                            op: BinOp::And,
                            left: Expression::Literal(Literal::Boolean(true)).into(),
                            right: Expression::UnaryOp {
                                op: UnaryOp::Not,
                                operand: Expression::Literal(Literal::Boolean(true)).into(),
                            }.into(),
                        }.into(),
                        right: Expression::Literal(Literal::Boolean(false)).into(),
                    },
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn binary_operation_boolean_comparison() {
            let test_source = "let x = !(5 > 3) && 7 <= 9 || 76 >= 99";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::BinOp {
                        op: BinOp::Or,
                        left: Expression::BinOp {
                            op: BinOp::And,
                            left: Expression::UnaryOp {
                                op: UnaryOp::Not,
                                operand: Expression::BinOp {
                                    op: BinOp::Greater,
                                    left: Expression::Literal(Literal::Integer(5)).into(),
                                    right: Expression::Literal(Literal::Integer(3)).into(),
                                }.into(),
                            }.into(),
                            right: Expression::BinOp {
                                op: BinOp::LesserEqual,
                                left: Expression::Literal(Literal::Integer(7)).into(),
                                right: Expression::Literal(Literal::Integer(9)).into(),
                            }.into(),
                        }.into(),
                        right: Expression::BinOp {
                            op: BinOp::GreaterEqual,
                            left: Expression::Literal(Literal::Integer(76)).into(),
                            right: Expression::Literal(Literal::Integer(99)).into(),
                        }.into(),
                    },
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }
    }

    mod identifier {
        use crate::ast::{BinOp, Expression, Literal, TopLevel, UnaryOp};
        use super::parse_source;

        #[test]
        fn underscore_identifier() {
            let test_source = "
                let _ = 5
                let _: Bool.t = true
            ";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "_".into(),
                    t: None,
                    expression: Expression::Literal(Literal::Integer(5)),
                },
                TopLevel::LetBinding {
                    identifier: "_".into(),
                    t: Some("Bool.t".into()),
                    expression: Expression::Literal(Literal::Boolean(true)),
                },
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn expression_with_identifiers() {
            let test_source = "
                let some_bool = true
                let some_int = 45
                let some_other_int = 187
                let x = !some_bool && some_int > some_other_int
            ";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "some_bool".into(),
                    t: None,
                    expression: Expression::Literal(Literal::Boolean(true)),
                },
                TopLevel::LetBinding {
                    identifier: "some_int".into(),
                    t: None,
                    expression: Expression::Literal(Literal::Integer(45)),
                },
                TopLevel::LetBinding {
                    identifier: "some_other_int".into(),
                    t: None,
                    expression: Expression::Literal(Literal::Integer(187)),
                },
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: None,
                    expression: Expression::BinOp {
                        op: BinOp::And,
                        left: Expression::UnaryOp {
                            op: UnaryOp::Not,
                            operand: Expression::Identifier("some_bool".to_string()).into(),
                        }.into(),
                        right: Expression::BinOp {
                            op: BinOp::Greater,
                            left: Expression::Identifier("some_int".to_string()).into(),
                            right: Expression::Identifier("some_other_int".to_string()).into(),
                        }.into(),
                    },
                }
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }

        #[test]
        fn expression_with_identifiers_explicitly_typed() {
            let test_source = "
                let some_bool: Bool.t = true
                let some_int: Int.t = 45
                let some_other_int : Int.t = 187
                let x: Bool.t = !some_bool && some_int > some_other_int
            ";
            let expected: Vec<TopLevel> = vec![
                TopLevel::LetBinding {
                    identifier: "some_bool".into(),
                    t: Some("Bool.t".into()),
                    expression: Expression::Literal(Literal::Boolean(true)),
                },
                TopLevel::LetBinding {
                    identifier: "some_int".into(),
                    t: Some("Int.t".into()),
                    expression: Expression::Literal(Literal::Integer(45)),
                },
                TopLevel::LetBinding {
                    identifier: "some_other_int".into(),
                    t: Some("Int.t".into()),
                    expression: Expression::Literal(Literal::Integer(187)),
                },
                TopLevel::LetBinding {
                    identifier: "x".into(),
                    t: Some("Bool.t".into()),
                    expression: Expression::BinOp {
                        op: BinOp::And,
                        left: Expression::UnaryOp {
                            op: UnaryOp::Not,
                            operand: Expression::Identifier("some_bool".to_string()).into(),
                        }.into(),
                        right: Expression::BinOp {
                            op: BinOp::Greater,
                            left: Expression::Identifier("some_int".to_string()).into(),
                            right: Expression::Identifier("some_other_int".to_string()).into(),
                        }.into(),
                    },
                },
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }
    }

    mod comment {
        use crate::ast::{Comment, Expression, Literal, TopLevel};
        use super::parse_source;

        #[test]
        fn expression_with_identifiers() {
            let test_source = "
                // Line comment
                let z = 4 // Line comment at end
                /// Documentation comment
                /* Block comment */
            ";
            let expected: Vec<TopLevel> = vec![
                TopLevel::Comment(Comment::LineComment("// Line comment".to_string())),
                TopLevel::LetBinding {
                    identifier: "z".into(),
                    t: None,
                    expression: Expression::Literal(Literal::Integer(4)),
                },
                TopLevel::Comment(Comment::LineComment("// Line comment at end".to_string())),
                TopLevel::Comment(Comment::DocComment("/// Documentation comment".to_string())),
                TopLevel::Comment(Comment::BlockComment("/* Block comment */".to_string())),
            ];
            let actual = parse_source(test_source);
            assert_eq!(expected, actual)
        }
    }
}